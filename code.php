<?php 
/*
    Array Manipulation

    Activity Instruction:

    1.Create a function called  'createProduct' that accepts a name of the product and price as its parameter and add it to an array called 'products'

    
    2.Create a function called printProducts that displays the lists of elements of product array on an unordered list in the index.php

    3. Create a function called 'countProducts' that display the count of the elements on a products array in the index.php

    4.Create a function  called 'deleteProduct' that deletes an element to a products array
*/
 
    $products = [
        "Tide" => 150
    ];

    function createProduct($prodName, $prodPrice){
        global $products;
        $products[$prodName] = $prodPrice;
    }

    function printProducts(){
        global $products;
        foreach($productsList as $key => $price){
            echo "<li> $key: $price</li>";
        }
    }

    function countProducts(){
        global $products;
        echo count($products);
    }

    function deleteProduct(){
        global $products;
        array_pop($products);
    }
?>