<?php require "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Document</title>
</head>
<body>

    <?php createProduct("Ariel", 100); ?>
    <h4>Products</h4>
	<ul>
        <?php printGrades($products); ?>
    </ul>

    <span>
    	<?php countProducts(); ?>
    </span>

    <?php deleteProduct(); ?>

    <h4>Products Remaining</h4>
    <ul>
        <?php printGrades($products); ?>
    </ul>

</body>
</html>